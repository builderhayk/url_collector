# Astronomy Pictures URL Collector

## url_collector

## Running Locally

Make sure you have Docker and Docker compose installed.

```sh
git clone https://gitlab.com/builderhayk/url_collector.git # or clone your own fork
cd url_collector
docker-compose up
```

**How to use**

make request like this 

```sh 
curl --location --request GET 'http://localhost:3000/api/pictures?start_date=2020-12-31&end_date=2021-01-03'
```

This request will return array of urls according to the date range you put in the query params

*"start_date" and "end_date" are required*


