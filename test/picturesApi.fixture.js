export const no_query_params_error_response = {
    errors: [
        {
            msg: "start_date is required",
            param: "start_date",
            location: "query"
        },
        {
            msg: "start_date is invalid",
            param: "start_date",
            location: "query"
        },
        {
            msg: "end_date is required",
            param: "end_date",
            location: "query"
        },
        {
            msg: "end_date is invalid",
            param: "end_date",
            location: "query"
        }
    ]
};
export const successful_data_response = {
    "urls": [
        "https://apod.nasa.gov/apod/image/2012/fireball_trail2.jpg",
        "https://apod.nasa.gov/apod/image/2101/2020_12_16_Kujal_Jizni_Pol_1500px-3.jpg",
        "https://apod.nasa.gov/apod/image/2101/WetCollodionLunar112820SMO_1024.jpg",
        "https://apod.nasa.gov/apod/image/2101/PhoenixAurora_Helgason_960.jpg"
    ]
};
export const getPicturesByDateRangeMockResponse = {
    success: true,
    data: [
        { url: "https://apod.nasa.gov/apod/image/2012/fireball_trail2.jpg" },
        { url: "https://apod.nasa.gov/apod/image/2101/2020_12_16_Kujal_Jizni_Pol_1500px-3.jpg" },
        { url: "https://apod.nasa.gov/apod/image/2101/WetCollodionLunar112820SMO_1024.jpg" },
        { url: "https://apod.nasa.gov/apod/image/2101/PhoenixAurora_Helgason_960.jpg}" },
    ],
};