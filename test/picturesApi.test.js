import chai from "chai";
import chaiHttp from "chai-http";
import sinon from "sinon";
import { describe, it } from "mocha";
import {
    getPicturesByDateRangeMockResponse,
    no_query_params_error_response,
    successful_data_response
} from "./picturesApi.fixture";
import { SUCCESS_CODE, VALIDATION_ERROR_CODE } from "../src/appConfig/status-codes";
import Server from "../src/app";
import UrlCollector from "../src/services/urlCollector.service";

const { app } = new Server();
const expect = chai.expect;

chai.should();

chai.use(chaiHttp);


describe("GET /pictures", () => {

    it("Should return validation errors", (done) => {
        chai.request(app)
            .get("/api/pictures/")
            .end((err, response) => {
                response.should.have.status(VALIDATION_ERROR_CODE);
                response.body.should.be.a("object");
                response.body.should.be.deep.equal(no_query_params_error_response);
                done();
            });
    });

    it("Should run successfully and return urls inside array", (done) => {
        const StubGetPicturesByDateRange = sinon.stub(UrlCollector, "getPicturesByDateRange");
        StubGetPicturesByDateRange.withArgs({
            start_date: "2020-12-31",
            end_date: "2021-01-03"
        }).returns(getPicturesByDateRangeMockResponse);
        chai.request(app)
            .get("/api/pictures?start_date=2020-12-31&end_date=2021-01-03")
            .end((err, response) => {
                response.should.have.status(SUCCESS_CODE);
                response.body.should.be.a("object");
                const sortedResponse = response.body.urls.sort();
                const urls = successful_data_response.urls.sort();
                expect(sortedResponse[0]).to.equal(urls[0]);
                done();
            });
    });

});
