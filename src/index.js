import App from "./app";
import {apiPort} from "./helpers/config";

const WebServer = new App();

WebServer.app.listen(apiPort || 8080, () => {
    // eslint-disable-next-line no-console
    console.log(`Server is running at ${apiPort}`);
});

export default WebServer;