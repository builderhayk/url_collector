import {
    apiUrl,
    appUrl,
    apiPort,
    corsOrigins,
    picturesApiKey,
    picturesApiUrl,
    requestsLimit
} from "../helpers/config";

const params = {
    development: {
        apiUrl,
        appUrl,
        apiPort,
        corsOrigins,
        picturesApiKey,
        picturesApiUrl,
        requestsLimit
    },
    production: {
        apiUrl,
        appUrl,
        apiPort,
        corsOrigins,
        picturesApiKey,
        picturesApiUrl,
        requestsLimit
    }
};

export default params[process.env.NODE_ENV || "development"];
