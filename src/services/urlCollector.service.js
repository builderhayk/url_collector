import { picturesApiKey, picturesApiUrl } from "../helpers/config";
import axios from "axios";

const UrlCollector = {
    async getPicturesByDateRange({ start_date, end_date }) {
        try {
            const response = await axios({
                url: picturesApiUrl,
                method: "get",
                params: {
                    api_key: picturesApiKey,
                    start_date,
                    end_date
                }
            });
            return {
                success: true,
                data: response.data,
            };
        } catch (e) {
            return {
                success: false,
                message: e.message,
                body: {
                    start_date,
                    end_date
                }
            };
        }
    }
};

export default UrlCollector;